const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const resolvers = require('./resolvers')
const schema = require('./schema')
const { ApolloServer } = require('apollo-server-express')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
const helmet = require('helmet')
app.use(helmet())

require('./db')
const routes = require('./routes')
app.use('/api', routes)


const server = new ApolloServer({
    typeDefs: schema,
    resolvers,
})

server.applyMiddleware({
    app,
    path: '/graphql',
})

module.exports = app