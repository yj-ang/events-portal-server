const { gql } = require('apollo-server-express')

module.exports = gql`
    extend type Query {
        customer(_id: ID!): Customer
        customers: [Customer]!
    }

    extend type Mutation {
        createCustomer(customerInput: CustomerInput!): Customer
        updateCustomer(customerInput: CustomerInput!): Customer
        deleteCustomer(_id: ID!): Boolean!
    }

    type Customer {
        _id: ID
        name: String!
        birthDay: String!
        gender: String!
        email: String
        nationality: String
        address: String
    }

    input CustomerInput {
        _id: ID
        name: String!
        birthDay: String!
        gender: String!
        email: String
        nationality: String
        address: String
    }
`;

