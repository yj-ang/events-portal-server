const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        blog(url: String!): Blog
        blogs: [Blog!]!
        someBlogs(limit: Int!, offset: Int!): [Blog!]!
        totalPages(blogPerPage: Int! = 10): Int!
    }

    extend type Mutation {
        createBlog(blogInput: BlogInput!): Blog!
        updateBlog(blogInput: BlogInput!): Boolean!
        deleteBlog(_id: ID!): Boolean!
    }

    type Blog {
        _id: ID
        title: String!
        url: String!
        created: String
        updated: String
        content: String!
        hidden: Boolean
    }

    input BlogInput {
        _id: ID
        title: String
        url: String
        content: String
        hidden: Boolean
    }
`;

