const { gql } = require('apollo-server-express')

const customerSchema = require('./customer')
const blogSchema = require('./blog')
const eventSchema = require('./event')

const linkSchema = gql`
    type Query {
        _: Boolean
    }
    type Mutation {
        _: Boolean
    }
    type Subscription {
        _: Boolean
    }
`;

module.exports = [linkSchema, customerSchema, blogSchema, eventSchema]

