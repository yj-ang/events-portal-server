const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        event(url: String!): Event
        events: [Event!]!
        someEvents(limit: Int!, offset: Int!): [Event!]!
        # totalPages(eventPerPage: Int! = 10): Int!
    }

    extend type Mutation {
        createEvent(eventInput: EventInput!): Event!
        updateEvent(eventInput: EventInput!): Boolean!
        deleteEvent(_id: ID!): Boolean!
    }

    type Event {
        _id: ID
        name: String!
        url: String!
        created: String
        updated: String
        imageUrl: String!
    }

    input EventInput {
        _id: ID
        name: String
        url: String
        imageUrl: String
    }
`;

