const express = require('express')
const cors = require('cors')
const { ApolloServer } = require('apollo-server-express')
const mongoose = require('mongoose')

const resolvers = require('./resolvers')
const schema = require('./schema')

const PORT = process.env.VUE_APP_APOLLO_ENGINE_PORT

const startServer = async () => {
    const app = express()
    app.use(cors())

    const server = new ApolloServer({
        typeDefs: schema,
        resolvers,
    })

    server.applyMiddleware({
        app,
        path: '/graphql',
    })

    mongoose.connection.on('error', (err) => {
        console.log('Could not connect to mongo server!')
        console.log(err)
    })

    try {
        console.log(process.env.MONGO_DB_CONN)
        await mongoose.connect(process.env.MONGO_DB_CONN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            socketTimeoutMS: 0,
            connectTimeoutMS: 0,
        })

        app.listen({ port: PORT }, () =>
            console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`)
        )
    } catch (error) {
        console.log(`[ERROR] ${error}`)
    }
}

startServer()