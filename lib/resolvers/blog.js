const blog = require('../models/blog')

module.exports = {
    Query: {
        blog: (parent, {url}) => blog.findOne({
            url
        }),
        blogs: () => blog.find(),
        someBlogs: (parent, {limit, offset}) => blog.find()
            .skip((offset-1)*limit)
            .limit(limit),
        totalPages: async (parent, {blogPerPage}) => {
            const count = await blog.count()
            
            return Math.ceil(count / blogPerPage) || 0
        }
    },
    Mutation: {
        createBlog: async (parent, {blogInput}, {models}) => {
            try {
                const blogPost = await blog.create({
                    title: blogInput.title,
                    content: blogInput.content,
                    url: blogInput.title.toLowerCase().replace(/ /g, '-')
                })

                return blogPost
            } catch (error) {
                console.log(error)
                return false
            }
        },
        updateBlog: async (parent, {blogInput}) => {
            try {
                const res = await blog.findOneAndUpdate({
                    _id: blogInput._id
                }, {
                    hidden: true,
                    updated: new Date().toISOString()
                })
                console.log(res)
                return true
            } catch (err) {
                console.error(err)
                return false
            }
        },
        deleteBlog: async (parent, {_id}) => {
            try {
                const res = await blog.findOneAndUpdate({_id}, {
                    hidden: true,
                    updated: new Date().toISOString()
                })
                console.log(res)
                return true
            } catch (err) {
                console.error(err)
                return false
            }
        }
    },
}