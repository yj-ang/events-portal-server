const customer = require('../models/customer')
const { ObjectId } = require('mongodb')

module.exports = {
    Query: {
        customer: (parent, {_id}) => customer.findOne(ObjectId(_id)),
        customers: () => customer.find(),
    },
    Mutation: {
        createCustomer: async (parent, {customerInput}) => {
            try {
                const cust = await customer.create(customerInput)

                return cust
            } catch (err) {
                console.log(err)
                switch (err.code) {
                    case 11000:
                        throw new Error('Duplicate key')
                    default:
                            throw new Error(err)
                }
            }
        },
        updateCustomer: async (parent, {customerInput}) => {
            try {
                const cust = await customer.findOneAndUpdate({
                    _id: customerInput._id
                }, customerInput, {new: true})

                return cust
            } catch (err) {
                console.log(err)
                throw new Error(err) 
            }
        },
        deleteCustomer: async (parent, {_id}) => {
            try {
                await customer.findOneAndDelete({_id})

                return true
            } catch (err) {
                console.log(err)
                throw new Error(err) 
            }
        }
    },
}