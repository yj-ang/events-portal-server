const event = require('../models/event')

module.exports = {
    Query: {
        event: (parent, {url}) => event.findOne({
            url
        }),
        events: () => event.find(),
        someEvents: (parent, {limit, offset}) => event.find()
            .skip((offset-1)*limit)
            .limit(limit),
        totalPages: async (parent, {eventPerPage}) => {
            const count = await event.count()
            
            return Math.ceil(count / eventPerPage) || 0
        }
    },
    Mutation: {
        createEvent: async (parent, {eventInput}, {models}) => {
            try {
                const eventPost = await event.create({
                    name: eventInput.name,
                    url: eventInput.url,
                    imageUrl: eventInput.imageUrl,
                })

                return eventPost
            } catch (error) {
                console.log(error)
                return false
            }
        },
        updateEvent: async (parent, {eventInput}) => {
            try {
                const res = await event.findOneAndUpdate({
                    _id: eventInput._id
                }, {
                    hidden: true,
                    updated: new Date().toISOString()
                })
                console.log(res)
                return true
            } catch (err) {
                console.error(err)
                return false
            }
        },
        deleteEvent: async (parent, {_id}) => {
            try {
                const res = await event.findOneAndUpdate({_id}, {
                    hidden: true,
                    updated: new Date().toISOString()
                })
                console.log(res)
                return true
            } catch (err) {
                console.error(err)
                return false
            }
        }
    },
}