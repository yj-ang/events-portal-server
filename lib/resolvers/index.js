const customerResolver = require('./customer')
const blogResolver = require('./blog')
const eventResolver = require('./event')

module.exports = [customerResolver, blogResolver, eventResolver]