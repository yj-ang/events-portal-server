const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const blogSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true,
    },
    url: {
        type: String,
        required: true,
        unique: true,
    },
    created: {
        type: String,
        default: new Date().toISOString(),
    },
    updated: {
        type: String,
        default: new Date().toISOString(),
    },
    content: {
        type: String,
        required: true,
    },
    hidden: {
        type: Boolean,
        default: false,
    }
})
module.exports = mongoose.model('blog', blogSchema);
