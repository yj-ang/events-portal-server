const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const eventSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    url: {
        type: String,
        required: true,
        unique: true
    },
    created: {
        type: String,
        default: new Date().toISOString()
    },
    updated: {
        type: String,
        default: new Date().toISOString()
    },
    imageUrl: {
        type: String,
        required: true
    },
});
module.exports = mongoose.model("event", eventSchema);
