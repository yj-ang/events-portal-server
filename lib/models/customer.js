const mongoose = require('mongoose')
const Schema = mongoose.Schema

const customerSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    birthDay: {
        type: String,
        required: true,
    },
    gender: {
        type: String,
        required: true,
    },
    email: {
        type: String,
    },
    nationality: {
        type: String,
    },
    address: {
        type: String,
    },
})
module.exports = mongoose.model('customer', customerSchema)
