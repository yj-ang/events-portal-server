const mongoose = require('mongoose')
mongoose.connect(process.env.DB, {
    useNewUrlParser: true
}).then(() => console.log('DB CONNECTED'))